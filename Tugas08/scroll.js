// SCROLL TO TOP
const scrollBtn = document.querySelector(".scroll-to-top");
scrollBtn.addEventListener("click", () => {
  document.documentElement.scrollTop = 0;
});
