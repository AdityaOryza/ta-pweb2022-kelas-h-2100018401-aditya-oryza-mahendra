// FUNGSI PADA FORM
function validateForm() {
  var namedpn = document.forms["myForm"]["namedpn"];
  var namebk = document.forms["myForm"]["namebk"];
  var telepon = document.forms["myForm"]["telepon"];
  var email = document.forms["myForm"]["email"];
  var message = document.forms["myForm"]["message"];

  if (namedpn.value == "") {
    document.getElementById("errorname").innerHTML = "nama tidak boleh kosong";
    namedpn.focus();
    return false;
  } else {
    document.getElementById("errorname").innerHTML = "";
  }
  if (namebk.value == "") {
    document.getElementById("errorname").innerHTML = "nama tidak boleh kosong";
    namebk.focus();
    return false;
  } else {
    document.getElementById("errorname").innerHTML = "";
  }
  if (telepon.value == "") {
    document.getElementById("errorname").innerHTML = "nama tidak boleh kosong";
    telepon.focus();
    return false;
  } else {
    document.getElementById("errorname").innerHTML = "";
  }

  if (email.value == "") {
    document.getElementById("erroremail").innerHTML =
      "email tidak boleh kosong";
    email.focus();
    return false;
  } else {
    document.getElementById("erroremail").innerHTML = "";
  }

  if (email.value.indexOf("@", 0) < 0) {
    document.getElementById("erroremail").innerHTML =
      "masukkan emial yang valid";
    email.focus();
    return false;
  }

  if (email.value.indexOf(".", 0) < 0) {
    document.getElementById("erroremail").innerHTML =
      "masukkan email yang valid";
    email.focus();
    return false;
  }

  if (message.value == "") {
    document.getElementById("errormsg").innerHTML = "pesan tidak boleh kosong";
    message.focus();
    return false;
  } else {
    document.getElementById("errormsg").innerHTML = "";
  }

  return true;
}
