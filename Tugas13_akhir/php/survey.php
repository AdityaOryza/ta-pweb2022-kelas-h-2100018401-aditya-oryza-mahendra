<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Required meta tags -->
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <!-- Font Awesome -->
    <script defer src="https://use.fontawesome.com/releases/v5.0.7/js/all.js"></script>

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat|Ubuntu" rel="stylesheet" />

    <!-- CSS Stylesheets -->
    <link rel="stylesheet" href="../css/form.css">
    <title>pesan</title>


</head>

<body>
    <div class="container">
        <h2 class="tittle">Terimakasih Sudah Mengisi Survey</h2>
        <p class="lead-text">Kritik dan saran anda akan sangat membantu kami</p>
        <img src="../images/thankyou.png" class="img" alt="thankyou-img" width="300px">
        <?php
    $field1 = $_POST['field1'];
    $field2 = $_POST['field2'];
    $field3 = $_POST['field3'];
    $field4 = $_POST['field4'];
    $field5 = $_POST['field5'];
    $fp = fopen("survey.txt","a+");
    fputs($fp,"$field1|$field2|$field3|$field4|$field5\n");
    fclose($fp);
    ?>
        <p class="lead-text">tekan tombon berikut ini untuk kembali ke homepage atau lihat pesan yang anda kirimkan</p>
        <a href="../index.html">
            <button>Homepage</button>
        </a>
        <a href="output_survey.php">
            <button>Lihat Pesan</button>
        </a>
    </div>
</body>

</html>