<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Required meta tags -->
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <!-- Font Awesome -->
    <script defer src="https://use.fontawesome.com/releases/v5.0.7/js/all.js"></script>

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat|Ubuntu" rel="stylesheet" />

    <!-- CSS Stylesheets -->
    <link rel="stylesheet" href="../css/output.css" />
    <title>Pesan Yang Masuk</title>

</head>

<body>
    <div class="container">
        <table border="1" cellspacing="0">
            <tr>
                <th colspan="6">PESAN DARI PENGUNJUNG
                    <br>
                    <p class="lead-text">pesan dari pengunjung akan tampil disini</p>
                </th>
            </tr>
            <tr>
                <td>Tanggal</td>
                <td>Nama Depan</td>
                <td>Nama Belakang</td>
                <td>Telepon</td>
                <td>Email</td>
                <td>Pesan</td>
            </tr>
            <tr>
                <?php
    $date=date('Y-m-d');
    $fp = fopen("pesan.txt", "r");
    while ($isi = fgets($fp,80)) {
        $pisah = explode('|', $isi);
        echo "<tr>  
                     <td>$date</td>
                     <td>$pisah[0]</td>
                     <td>$pisah[1]</td>
                     <td>$pisah[2]</td>
                     <td>$pisah[3]</td>
                     <td>$pisah[4]</td>
            </tr>";
            }
            ?>
            </tr>
        </table>
    </div>

    <div class="container text-center">
        <a href='../index.html'>
            <button class="btn btn-outline-dark mt-5 mb-5">HOMEPAGE</button>
        </a>
        <hr>
    </div>
</body>

</html>