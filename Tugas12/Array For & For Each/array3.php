<?php
$arrWarna = array("Red","Orange","Yellow","Green","Blue","Purple");

echo "Menampilkan Isi Array Dengan For : <br>";
for ($i=0; $i < count($arrWarna); $i++){
    echo "Warna Pelangi <font color=$arrWarna[$i]> ".$arrWarna[$i]."</font><br>";    
}
echo "<br>Menampilkan Isi Array Dengan ForEach : <br>";
foreach($arrWarna as $warna){
    echo "Warna Pelangi <font color=$warna>".$warna."</font><br>";
}
?>