<?php
echo "Daftar nilai Pweb kelas H <br>";
$arrNilai = array("Adit" => 80, "anggi" => 75, "bita" => 50, "alex" => 100);
echo "Menampilkan isi array assosiatif dengan foreach : <br>";
foreach ($arrNilai as $nama => $nilai) {
    echo "Nilai $nama = $nilai<br>";
}

reset($arrNilai);
echo "<br>Menampilkan isi array asosiatif dengan WHILE dan LIST : <br>";
while (list($nama, $nilai) = each($arrNilai)) {
    echo "Nilai $nama = $nilai<br>";
}